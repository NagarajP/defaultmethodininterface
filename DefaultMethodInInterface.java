package com.myzee.defaultmethodininterface;

public class DefaultMethodInInterface {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Interface1 i = new Test();
		i.defMethod();
	}

}

interface Interface1 {
	default public void defMethod() {
		System.out.println("In default method");
	}
}


class Test implements Interface1 {
		   
	  public void defMethod() { 
		  System.out.println("in test default method"); 
	  }
}