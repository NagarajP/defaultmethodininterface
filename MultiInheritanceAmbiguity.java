package com.myzee.defaultmethodininterface;


/*
 * Using default methods in interfaces, we may result in getting multiple inheritance ambiguity 
 * error.
 * To solve this, we should override one of the methods, from Interface2 or Interface3, 
 * in MultiInheritanceAmbiguity class.
 */
public class MultiInheritanceAmbiguity implements Interface2, Interface3{

	public static void main(String[] args) {
		
	}

}

interface Interface2 {
	default public int add() {
		return (1+2);
	}
}

interface Interface3 {
	default public int add() {
		return (3+4);
	}
}